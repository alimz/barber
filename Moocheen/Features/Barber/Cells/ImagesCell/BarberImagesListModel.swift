//
//  BarberImagesListModel.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/21/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

struct BarberImagesListModel {
    let title: String
    let items: [String]
}

extension String: HorizontalListItem {}
