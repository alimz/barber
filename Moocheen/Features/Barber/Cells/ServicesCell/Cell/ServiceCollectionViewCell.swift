//
//  ServiceCollectionViewCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/20/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell, HorizontalListCell, ComponentProtocol {

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var  titleLabel: UILabel!

    func fill(with item: HorizontalListItem, gender: Gender) {
        guard let service = item as? BarberServiceItem else { return }
        if let imageURL = service.imageURL?.imageDownloadURL {
            imageView.downloadImage(url: imageURL)
        }
        titleLabel.text = service.title

    }
}
