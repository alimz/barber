//
//  UIFontExtension.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

enum DefaultFontType: String {
    case regular = "IRANSans"
    case medium = "IRANSans-Medium"
    case light = "IRANSans-Light"
}

extension UIFont {
    convenience init(_ type: DefaultFontType, withSize size: CGFloat) {
        self.init(name: type.rawValue, size: size)!
    }

    static var defaultFont: UIFont {
        return UIFont(.regular, withSize: 16)
    }
}
