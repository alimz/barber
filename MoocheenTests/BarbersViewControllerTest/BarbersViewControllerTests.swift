//
//  HomeViewControllerTests.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class BarbersViewControllerTests: MoocheenTestCase {
    var vc: BarbersViewController?

    override func setUp() {
        guard let mock = loadMock(fileName: "HomeMock") else {
            XCTFail("Could'nt load mock file")
            return
        }

        let mockService = ServicesMock(multiResponse: [RequestURL.getHomePage.path: (mock, nil)])
        vc = BarbersViewController(service: mockService)
        let _ = vc!.view
    }

    func testInitWithCoder() {
        let archiver = NSKeyedArchiver(requiringSecureCoding: true)
        let brandVC = BarbersViewController(coder: archiver)
        XCTAssertNil(brandVC)
    }

    func testTableViewDataSource() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertEqual(self.vc?._tableView.numberOfSections, 1)
            XCTAssertEqual(self.vc?._tableView.numberOfRows(inSection: 0), 2)
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }

    func testBarberItemCell() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let tableCell = self.vc?._tableView.cellForRow(at: IndexPath(item: 0, section: 0)) as? BarbersListCell
            XCTAssertNotNil(tableCell)

            XCTAssertEqual(tableCell!.horizontalList.titleLable.text, "Best barbers".localized)

            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }
}
