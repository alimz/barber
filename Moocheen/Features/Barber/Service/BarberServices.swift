//
//  BarberServices.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation


extension BaseServices {
    func getBarberPage(barberID: String, completion: @escaping (Result<BarberProfile, Error>) -> Void) {
        let path = RequestURL.getBarberPage.path
        let params = ["barber": barberID]
        sendRequest(path, method: .get, parameters: params) { response in //TODO: Change method to .post
            guard let data = response.data, response.error == nil else {
                completion(.failure(response.error!))
                return
            }

            do {
                let result: BarberProfile = try JSONDecoder().decode(BarberProfile.self, from: data)
                completion(.success(result))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
