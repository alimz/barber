//
//  LoginViewController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func loginViewController(_ vc: LoginViewController, didLoginWithTempUser user: User)
}

class LoginViewController: BaseViewController {

    override init(service: ServicesProtocol) {
        super.init(service: service)
    }

    required init?(coder _: NSCoder) {
        return nil
    }

    var gender: Gender? {
        if maleButton.isSelected {
            return .male
        } else if femaleButton.isSelected {
            return .female
        }
        return nil
    }

    @IBOutlet weak var fullnameField: TextFieldWithIcon!
    @IBOutlet weak var phoneField: TextFieldWithIcon!

    @IBOutlet weak var femaleButton: IconButton!
    @IBOutlet weak var maleButton: IconButton!

    weak var delegate: LoginViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    fileprivate func setupUI() {
        femaleButton.gender = .female
        maleButton.gender = .male
        hideKeyboardWhenTappedAround()
        femaleButton.isSelected = true
        phoneField.textField.keyboardType = .phonePad
        if #available(iOS 13.0, *) {
            fullnameField.fill(placeholder: "Fullname".localized, icon: UIImage(systemName: "photo")?.withTintColor(UIColor(namedColor: .GreyPrimary)))
            phoneField.fill(placeholder: "Phone number".localized, icon: UIImage(systemName: "phone")?.withTintColor(UIColor(namedColor: .GreyPrimary)))

        } else {
            // Fallback on earlier versions
        }
    }

    @IBAction func maleButtonAction(_ sender: IconButton) {
        sender.isSelected = true
        if femaleButton.isSelected { femaleButton.isSelected = false }
        navigationController?.navigationBar.barTintColor = UIColor.mainColor(for: .male)
        changeGender(to: .male)
    }

    @IBAction func femaleButtonAction(_ sender: IconButton) {
        sender.isSelected = true
        if maleButton.isSelected { maleButton.isSelected = false }
        navigationController?.navigationBar.barTintColor = UIColor.mainColor(for: .female)
        changeGender(to: .female)
    }

    @IBAction func registerAction(_ sender: CurvedButton) {
        guard let gender = gender else { return } //TODO: Should select gender
        guard let phone = phoneField.text else { return }
        service.login(phone: phone) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                print(data)
                let tempUser = User(authenticationToken: nil, gender: gender, phone: phone)
                self.delegate?.loginViewController(self, didLoginWithTempUser: tempUser)
            default: return //TODO: Show error
            }
        }
    }
}
