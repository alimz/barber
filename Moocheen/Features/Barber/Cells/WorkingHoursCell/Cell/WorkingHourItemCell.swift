//
//  WorkingHourItemCell.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class WorkingHourItemCell: UICollectionViewCell, HorizontalListCell, ComponentProtocol {

    @IBOutlet private weak var weekdayLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!

    var item: WorkingHourItem? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func awakeFromNib() {
        backgroundColor = UIColor(namedColor: .WhitePrimary)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableView.automaticDimension
        tableView.isScrollEnabled = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TimeTableViewCell.self, forCellReuseIdentifier: "WorkingHourItemTimeCell")
    }

    func fill(with item: HorizontalListItem, gender: Gender) {
        guard let item = item as? WorkingHourItem else { return }
        self.item = item
        self.weekdayLabel.text = item.weekday
    }
}

extension WorkingHourItemCell: TableViewDatasourceDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item?.times?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let times = self.item?.times else { return UITableViewCell() }
        guard let time = times[safe: indexPath.row] else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkingHourItemTimeCell") as! TimeTableViewCell
        cell.fill(with: time)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
}

class TimeTableViewCell: UITableViewCell {
    var defaultAccessoryType: AccessoryType = .none

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textLabel?.textAlignment = .center
        textLabel?.font = .init(.light, withSize: 13)
        textLabel?.numberOfLines = 1
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func fill(with item: String?) {
        textLabel?.text = item
    }
}
