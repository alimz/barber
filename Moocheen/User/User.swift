//
//  User.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

enum Gender: String, Codable {
    case male
    case female
}

struct User: Codable {
    var authenticationToken: String? {
        didSet {
            save()
        }
    }

    var gender: Gender? {
        didSet {
            save()
        }
    }

    var phone: String? {
        didSet {
            unifiedPhone()
        }
    }

    enum CodingKeys: String, CodingKey {
        case authenticationToken, phone, gender
    }

    static var serviceUser: User {
        guard let user = User.savedUser else {
            return .empty
        }
        return user
    }

    static var savedUser: User? {
        if let savedUser = UserDefaults.standard.object(forKey: .user) as? Data {
            if let user = try? JSONDecoder().decode(User.self, from: savedUser) {
                return user
            }
        }
        return nil
    }

    func save() {
        if let encoded = try? JSONEncoder().encode(self) {
            UserDefaults.standard.set(encoded, forKey: .user)
        }
    }

    private mutating func unifiedPhone() {
        if let phone = phone, !phone.hasPrefix("0") { self.phone = "0\(phone)" }
    }
}

extension User {
    static var empty: User {
        User(authenticationToken: nil, gender: nil, phone: nil)
    }
}
