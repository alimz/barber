//
//  WorkingHoursModel.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

struct WorkingHourItem: HorizontalListItem, Codable {
    let weekday: String?
    let times: [String]?
}

struct WorkingHoursModel {
    let title: String
    let items: [WorkingHourItem]
}
