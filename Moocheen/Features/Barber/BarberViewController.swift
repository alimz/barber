//
//  BarberViewController.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/20/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BarberViewController: BaseViewController, TableViewProtocol {

    @IBOutlet private weak var topFillerView: UIView!
    @IBOutlet private weak var headerView: UserHeaderView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var orderButton: NormalButton!

    var _tableView: UITableView { tableView }

    let barberID: String
    var profile: BarberProfile? {
        didSet {
            reloadData()
            configHeader()
        }
    }

    init(barberID: String, service: ServicesProtocol) {
        self.barberID = barberID
        super.init(service: service)
    }

    required init?(coder _: NSCoder) {
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView(cellIdentifiers: ["BarberServicesCell",
                                         "BarberImagesListCell",
                                         "WorkingHoursCell"])
        getBarberProfile()
        configTopFillerView()
    }

    fileprivate func configTopFillerView() {
        topFillerView.backgroundColor = UIColor.mainColor(for: service.user.gender ?? .female)
    }

    fileprivate func configHeader() {
        guard let barber = profile else { return }
        headerView.fill(with: barber)
    }

    fileprivate func getBarberProfile() {
        service.getBarberPage(barberID: barberID) { [weak self] result in
            switch result {
            case .success(let barberProfile):
                self?.profile = barberProfile
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension BarberViewController: TableViewDatasourceDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            return getServicesCell()
        case 1:
            return getImagesCell()
        case 2:
            return getWorkingHoursCell()
        default:
            return UITableViewCell()
        }
    }

    func getServicesCell() -> UITableViewCell {
        guard let items = profile?.services else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarberServicesCell") as! BarberServicesCell
        let serviceModel = BarberServiceModel(title: "Services".localized, items: items)
        cell.fill(with: serviceModel, gender: service.user.gender)
        return cell
    }

    func getImagesCell() -> UITableViewCell {
        guard let items = profile?.sampleList else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarberImagesListCell") as! BarberImagesListCell
        let imagesModel = BarberImagesListModel(title: "Images".localized, items: items)
        cell.fill(with: imagesModel, gender: service.user.gender)
        return cell
    }

    func getWorkingHoursCell() -> UITableViewCell {
        guard let items = profile?.workingHours else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursCell") as! WorkingHoursCell
        let workingHoursModel = WorkingHoursModel(title: "Working hours".localized, items: items)
        cell.fill(with: workingHoursModel, gender: service.user.gender)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
