//
//  BarberImagesListCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/21/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol BarberImagesListCellDelegate: class {
    func barberImagesListCell(_ cell: BarberImagesListCell, didSelectImageWithURL imageURL: String)
}

class BarberImagesListCell: UITableViewCell {

    @IBOutlet private weak var horizontalList: HorizontalCollectionTableViewCell!

    weak var delegate: BarberImagesListCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        horizontalList.setupCell(collectionCellIdentifier: "BarberImageItemCell")
        horizontalList.delegate = self
    }

    func fill(with item: BarberImagesListModel, gender: Gender?) {
        let config = HorizontalCellConfig(item: HorizontalListStruct(title: item.title, items: item.items), collectionCellsize: CGSize(width: 170, height: 120), data: nil, gender: gender, needsMoreButton: false)
        horizontalList.fill(config: config)
    }
}

extension BarberImagesListCell: HorizontalCollectionCellDelegate {
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didSelectItem item: HorizontalListItem) {
        guard let imageURL = item as? String else { return }
        delegate?.barberImagesListCell(self, didSelectImageWithURL: imageURL)
    }
}
