//
//  LoginViewControllerTests.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class LoginViewControllerTests: MoocheenTestCase, LoginViewControllerDelegate {

    func loginViewController(_ vc: LoginViewController, didLoginWithTempUser user: User) {
        expectation.fulfill()
    }

    var vc: LoginViewController?

    override func setUp() {
        let mockService = ServicesMock(response: ("[\"1234\"]", nil))
        vc = LoginViewController(service: mockService)
        vc?.delegate = self
        let _ = vc!.view
    }

    func testInitWithCoder() {
        let archiver = NSKeyedArchiver(requiringSecureCoding: true)
        let brandVC = LoginViewController(coder: archiver)
        XCTAssertNil(brandVC)
    }

    func testSelectMaleButton() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.vc?.maleButtonAction(self.vc!.maleButton)
            XCTAssertFalse(self.vc?.femaleButton.isSelected ?? true)
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }

    func testSelectFemaleButton() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.vc?.femaleButtonAction(self.vc!.femaleButton)
            XCTAssertFalse(self.vc?.maleButton.isSelected ?? true)
            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }

    func testClickRegister() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.vc?.fullnameField.text = "Test user"
            self.vc?.phoneField.text = "09123456789"
            self.vc?.maleButtonAction(self.vc!.maleButton)
            self.vc?.registerAction(CurvedButton())
        }
        wait(for: [expectation], timeout: 2)
    }
}
