//
//  FilterModel.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

struct FilterModel {
    let serviceIDs: [Int]
    let minimumPrice: Double
    let maximumPrice: Double
}
