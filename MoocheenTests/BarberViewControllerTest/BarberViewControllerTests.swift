//
//  BarberViewControllerTests.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class BarberViewControllerTests: MoocheenTestCase {
    var vc: BarberViewController?

    override func setUp() {
        guard let mock = loadMock(fileName: "BarberMock") else {
            XCTFail("Could'nt load mock file")
            return
        }

        let mockService = ServicesMock(multiResponse: [RequestURL.getBarberPage.path: (mock, nil)])
        vc = BarberViewController(barberID: "fake_id", service: mockService)
        let _ = vc!.view
    }

    func testInitWithCoder() {
        let archiver = NSKeyedArchiver(requiringSecureCoding: true)
        let brandVC = BarberViewController(coder: archiver)
        XCTAssertNil(brandVC)
    }

    func testBarberServices() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let tableCell = self.vc?._tableView.cellForRow(at: IndexPath(item: 0, section: 0)) as? BarberServicesCell
            XCTAssertNotNil(tableCell)

            XCTAssertEqual(tableCell!.horizontalList.titleLable.text, "Services".localized)

            self.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }
}
