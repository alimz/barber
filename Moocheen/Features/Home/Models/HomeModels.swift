//
//  HomeModels.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

struct HomeModel: Codable {
    let bestBarbers: [Barber]?
    let closestBarbers: [Barber]?

    enum CodingKeys: String, CodingKey {
        case bestBarbers = "best_barbers"
        case closestBarbers = "closest_barbers"
    }
}
