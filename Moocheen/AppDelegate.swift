//
//  AppDelegate.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainFlowController: MainFlowController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //        setupMainFlowController()
        setGlobalAppearance()
        return true
    }

    fileprivate func setupMainFlowController() {
        window = UIWindow(frame: UIScreen.main.bounds)

        let flowController = MainFlowController()
        window?.rootViewController = flowController
        mainFlowController = flowController
        window?.makeKeyAndVisible()
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }

    func setGlobalAppearance() {
        UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont(.regular, withSize: 10)], for: .normal)

        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.font: UIFont.defaultFont]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchController.self]).defaultTextAttributes = [NSAttributedString.Key.font: UIFont.defaultFont]

        UIBarButtonItem.appearance().setTitleTextAttributes([.font: UIFont(.regular, withSize: 17)], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([.font: UIFont(.regular, withSize: 17)], for: .highlighted)
    }


}

