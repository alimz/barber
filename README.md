**There are two mock files for sake of testing project:**

1. BarberMock/main-page.json
2. BarberMock/barber.json

**Stpes to serve this mocks:**

1. *cd* into BarberMock directory
2. run this command on terminal: `python3 -m http.server 8000`

**Screenshots:**

Intro: First Screen            |  Intro: Last Screen
:-------------------------:|:-------------------------:
![](Screenshots/1.png)  |  ![](Screenshots/2.png)

Login: Female            |  Login: Male
:-------------------------:|:-------------------------:
![](Screenshots/3.png)  |  ![](Screenshots/4.png)

Verification Code           |  Home View
:-------------------------:|:-------------------------:
![](Screenshots/5.png)  |  ![](Screenshots/6.png)

Barber Details           |  
:-------------------------:
![](Screenshots/7.png)  |  