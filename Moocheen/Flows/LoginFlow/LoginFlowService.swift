//
//  LoginFlowService.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 4/26/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import Alamofire

extension BaseServices {
    func login(phone: String, completion: @escaping (_ result: Result<[String], Error>) -> Void) {
        let path = RequestURL.register(phone).path
        sendRequest(path) { result in
            guard let data = result.data else {
                completion(.failure(result.error!))
                return
            }

            if let response = try? JSONDecoder().decode([String].self, from: data) {
                completion(.success(response))
            } else {
                completion(.failure(result.error!))
            }
        }
    }

    func loginAuth(phone: String, code: String, completion: @escaping (_ result: Result<AuthResponse, Error>) -> Void) {
        let params = ["phone": phone, "code": code]
        let path = RequestURL.loginAuth.path
        sendRequest(path, method: .post, parameters: params) { result in
            guard let data = result.data else {
                completion(.failure(result.error!))
                return
            }

            do {
                let response = try JSONDecoder().decode(AuthResponse.self, from: data)
                completion(.success(response))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
