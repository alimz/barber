//
//  ServicesProtocol.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import Alamofire

protocol ServicesProtocol: LoginFlowServiceProtocol, HomeServicesProtocol, BarberServicesProtocol {
    var user: User { get set }
    var isAuthenticated: Bool { get }

    @discardableResult
    func sendRequest(_ url: String,
                     method: HTTPMethod,
                     parameters: Parameters?,
                     imageData: Data?,
                     completion: @escaping (_ response: DataResponse<Any, AFError>) -> Void) -> DataRequest?
}

extension ServicesProtocol {
    var isAuthenticated: Bool { user.authenticationToken != nil }
}


protocol LoginFlowServiceProtocol {
    func login(phone: String, completion: @escaping (_ result: Result<[String], Error>) -> Void)
    func loginAuth(phone: String, code: String, completion: @escaping (_ result: Result<AuthResponse, Error>) -> Void)
}

protocol HomeServicesProtocol {
    func getBestBarbers(offset: Int, completion: @escaping (_ result: Result<[Barber], Error>) -> Void)
    func getClosestBarbers(offset: Int, completion: @escaping (_ result: Result<[Barber], Error>) -> Void)
    func getHomePage(completion: @escaping (_ result: Result<HomeModel, Error>) -> Void)
    func serachBarbers(name: String, completion: @escaping (_ result: Result<[Barber], Error>) -> Void)
    func filterBarbers(filter: FilterModel, completion: @escaping (_ result: Result<[Barber], Error>) -> Void)
}

protocol BarberServicesProtocol {
    func getBarberPage(barberID: String, completion: @escaping (_ result: Result<BarberProfile, Error>) -> Void)
}
