//
//  IntroViewController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol IntroViewControllerDelegate: class {
    func introViewControllerDidFinishShowing(_ introVC: IntroViewController)
}

struct IntroItem {
    let backgroundImage: UIImage?
    let itemIcon: UIImage?
    let itemTitle: String
    let itemDescription: String
}

class IntroViewController: BaseViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var nextButton: CurvedButton!

    weak var delegate: IntroViewControllerDelegate?

    private var items = [IntroItem]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    private var isLastPage: Bool {
        pageControl.currentPage >= pageControl.numberOfPages - 1
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fillItems()
        setupCollectionView()
        updateBottomButton()
    }

    fileprivate func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "IntroCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "IntroCollectionViewCell")
    }

    fileprivate func fillItems() {
        let menBarber = IconBuilder(.menBarber).build()
        let womenBarber = IconBuilder(.womenBarber).build()
        let scissorsIcon = IconBuilder(.scissors).build()
        let eyelashIcon = IconBuilder(.eyelash).build()
        let makeupIcon = IconBuilder(.makeup).build()

        items = [
            IntroItem(backgroundImage: menBarber, itemIcon: scissorsIcon, itemTitle: "Men barbershop".localized, itemDescription: "Lorem ipsum".localized),
            IntroItem(backgroundImage: womenBarber, itemIcon: eyelashIcon, itemTitle: "Women barbershop".localized, itemDescription: "Lorem ipsum".localized),
            IntroItem(backgroundImage: menBarber, itemIcon: makeupIcon, itemTitle: "Makeup".localized, itemDescription: "Lorem ipsum".localized)
        ]
        pageControl.numberOfPages = items.count
    }

    @IBAction func nextPageButtonAction(_ sender: UIButton) {
        if isLastPage {
            delegate?.introViewControllerDidFinishShowing(self)
        } else {
            goToNextPage()
        }
    }
}

extension IntroViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
        cell.fill(withItem: items[indexPath.item])
        return cell
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updatePageControl(scrollView: scrollView)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        updatePageControl(scrollView: scrollView)
    }

    fileprivate func updatePageControl(scrollView: UIScrollView) {
        pageControl.currentPage = (items.count - 1) - Int(scrollView.contentOffset.x) / Int(collectionView.frame.size.width)
        updateBottomButton()
    }

    fileprivate func updateBottomButton() {
        let title = isLastPage ? "Start".localized : "Next".localized
        let bgColor = isLastPage ? UIColor(namedColor: .ButtonNormal) : UIColor(namedColor: .WhitePrimary)
        let textColor = isLastPage ? UIColor(namedColor: .WhitePrimary) : UIColor(namedColor: .GreyPrimary)
        nextButton.setTitle(title, for: .normal)

        UIView.animate(withDuration: 0.1) {
            self.nextButton.setTitleColor(textColor, for: .normal)
            self.nextButton.backgroundColor = bgColor
        }
    }

    fileprivate func goToNextPage() {
        let nextIndex = IndexPath(item: pageControl.currentPage + 1, section: 0)
        collectionView.scrollToItem(at: nextIndex, at: .centeredHorizontally, animated: true)
    }

    fileprivate func goToPriviousPage() {
        let previousIndex = IndexPath(item: pageControl.currentPage - 1, section: 0)
        collectionView.scrollToItem(at: previousIndex, at: .centeredHorizontally, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}
