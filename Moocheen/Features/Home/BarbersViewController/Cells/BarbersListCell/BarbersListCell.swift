//
//  BarbersListCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol BarbersListCellDelegate: class {
    func babersListCell(_ cell: BarbersListCell, didSelectBarber barber: Barber)
    func babersListCell(_ cell: BarbersListCell, didRequestShowMoreItemsForType type: BarbersType)
}

class BarbersListCell: UITableViewCell {

    @IBOutlet weak var horizontalList: HorizontalCollectionTableViewCell!

    private var type: BarbersType?
    weak var delegate: BarbersListCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        horizontalList.setupCell(collectionCellIdentifier: "BarberCollectionViewCell")
        horizontalList.delegate = self
    }

    func fill(with item: BarbersListItem, gender: Gender?) {
        let config = HorizontalCellConfig(item: HorizontalListStruct(title: item.title, items: item.barbers), collectionCellsize: CGSize(width: 200, height: 180), data: type, gender: gender, needsMoreButton: true)
        horizontalList.fill(config: config)
    }
}

extension BarbersListCell: HorizontalCollectionCellDelegate {
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didSelectItem item: HorizontalListItem) {
        guard let barber = item as? Barber else { return }
        delegate?.babersListCell(self, didSelectBarber: barber)
    }

    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didRequestShowMoreItemsWithData data: Any?) {
        guard let type = data as? BarbersType else { return }
        delegate?.babersListCell(self, didRequestShowMoreItemsForType: type)
    }
}
