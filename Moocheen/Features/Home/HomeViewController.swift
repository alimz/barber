//
//  HomeViewController.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet private weak var stackView: UIStackView!

    lazy var barbersViewController: BarbersViewController = {
        BarbersViewController(service: service)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        addChilds()
        setupNavigationBar()
    }

    func addChilds() {
        stackView.addChild(in: self, childViewController: barbersViewController)
    }

    fileprivate func setupNavigationBar() {
        
    }

}
