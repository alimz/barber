//
//  UIStackViewExtension.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {

    func addChild(in parentViewController: UIViewController, childViewController: ComponentViewContorller) {
        parentViewController.addChild(childViewController)
        parentViewController.view.addSubview(childViewController.view)
        parentViewController.view.backgroundColor = UIColor(namedColor: .WhitePrimary)
        childViewController.didMove(toParent: parentViewController)
        UIView.performWithoutAnimation {
            addArrangedSubview(childViewController.view)
        }
    }

    func addChilds(in parentViewController: UIViewController, childViewControllers: [ComponentViewContorller]) {
        childViewControllers.forEach { (child) in
            addChild(in: parentViewController, childViewController: child)
        }
    }

    func hideViewsWithAnimation(views: [UIView], bool: Bool) {
        if views.allSatisfy({ $0.isHidden == bool }) { return }
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: [], animations: {
            views.forEach {
                $0.isHidden = bool
            }
            self.layoutIfNeeded()
        }, completion: nil)
    }

    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
