//
//  BarberCollectionViewCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BarberCollectionViewCell: UICollectionViewCell, HorizontalListCell, ComponentProtocol {

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    @IBOutlet private weak var starsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func fill(with item: HorizontalListItem, gender: Gender) {
        guard let barber = item as? Barber else { return }
        changeGender(to: gender)
        if let imageURL = barber.imageURL?.imageDownloadURL {
            imageView.downloadImage(url: imageURL)
        }
        nameLabel.text = barber.name
        distanceLabel.text = "۵ کیلومتر"
        starsLabel.text = "۳ ستاره"
    }
}
