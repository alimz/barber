//
//  BaseFlowController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol FinishFlowDelegate: class {
    func didFinishFlow(_ vc: UIViewController)
}

class BaseFlowController: NSObject {
    weak var navigationController: NavigationController?
    var service: ServicesProtocol

    init(service: ServicesProtocol) {
        self.service = service
    }

    func initialViewController() -> UIViewController {
        return UIViewController()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension BaseFlowController {
}
