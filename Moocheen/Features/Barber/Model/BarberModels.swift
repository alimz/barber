//
//  BarberModels.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

enum BarberGender: String, Codable {
    case male = "m"
    case female = "f"
}

struct BarberProfile: Codable {
    let firstName: String?
    let lastName: String?
    let gender: BarberGender?
    let address: String?
    let score: Int?
    let profileImage: String?
    let sampleList: [String]?
    let workingHours: [WorkingHourItem]?
    let barberName: String?
    let services: [BarberServiceItem]?

    var fullName: String? {
        "\(firstName ?? "") \(lastName ?? "")"
    }

    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case gender
        case address
        case score = "point"
        case profileImage = "image"
        case sampleList = "sample_list"
        case workingHours = "working_hours"
        case barberName
        case services
    }
}
