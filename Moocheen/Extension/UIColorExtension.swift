//
//  UIColorExtension.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

enum ColorPalette: String, CaseIterable {
    case Background

    case ButtonNormal

    case WhitePrimary
    case BlackPrimary
    case BrandPrimary

    case GreyHint
    case GreyPrimary
    case GreySecondary
    case GreyBackground

    case FemalePink
    case FemaleGradientBase
    case MaleBlue
    case MaleGradientBase
}

extension Gender {
    var gradientColors: [UIColor] {
        switch self {
        case .male: return [UIColor(namedColor: .MaleGradientBase), UIColor(namedColor: .MaleBlue)]
        case .female: return [UIColor(namedColor: .FemaleGradientBase), UIColor(namedColor: .FemalePink)]
        }
    }
}

enum UIColorStyle {
    case unspecified
    case dark
    case light
}

extension UIColor {
    convenience init(namedColor: ColorPalette, style: UIColorStyle = .unspecified) {
        switch style {
        case .unspecified:
            self.init(named: namedColor.rawValue)!
        case .dark:
            let color = UIColor(named: namedColor.rawValue)!
            self.init(cgColor: color.darkMode.cgColor)
        case .light:
            let color = UIColor(named: namedColor.rawValue)!
            self.init(cgColor: color.lightMode.cgColor)
        }
    }

    var darkMode: UIColor {
        if #available(iOS 13.0, *) {
            return self.resolvedColor(with: UITraitCollection(userInterfaceStyle: .dark))
        } else {
            return self
        }
    }

    var lightMode: UIColor {
        if #available(iOS 13.0, *) {
            return self.resolvedColor(with: UITraitCollection(userInterfaceStyle: .light))
        } else {
            return self
        }
    }

    static func mainColor(for gender: Gender) -> UIColor {
        return gender == .female ? UIColor(namedColor: .FemalePink) : UIColor(namedColor: .MaleBlue)
    }
}

