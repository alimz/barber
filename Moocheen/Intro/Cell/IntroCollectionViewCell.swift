//
//  IntoCollectionViewCell.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/14/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class IntroCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var itemIconImageView: UIImageView!
    @IBOutlet private weak var itemTitleLabel: UILabel!
    @IBOutlet private weak var itemDescriptionLabel: UILabel!

    func fill(withItem item: IntroItem) {
        backgroundImageView.image = item.backgroundImage
        itemIconImageView.image = item.itemIcon
        itemTitleLabel.text = item.itemTitle
        itemDescriptionLabel.text = item.itemDescription
    }

}
