//
//  MockService.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Alamofire
@testable import Moocheen

protocol MockServiceProtocol {
    func request(for url: String, parameters: [String: Any]?)
}

class ServicesMock: BaseServices {
    var delegate: MockServiceProtocol?

    var responseMap: [String: [(string: String?, error: NSError?)]]

    var delay: TimeInterval = 0

    convenience init(response: (String?, NSError?)) {
        self.init(multiResponse: ["": response])
    }

    init(multiResponses: [String: [(String?, NSError?)]]) {
        responseMap = multiResponses
        super.init(user: .empty)
    }

    convenience init(multiResponse: [String: (String?, NSError?)]) {
        let responseMap = multiResponse.mapValues({ (value) -> [(string: String?, error: NSError?)] in
            let (string, error) = value
            return [(string: string, error: error)]
        })
        self.init(multiResponses: responseMap)
    }

    override func sendRequest(_ url: String, method: HTTPMethod = .get, parameters: Parameters? = nil, imageData: Data? = nil, completion: @escaping (AFDataResponse<Any>) -> Void) -> DataRequest? {
        var response: (string: String?, error: NSError?)
        if responseMap[url] != nil {
            response = responseMap[url]!.popLastExcludeLastOne()!
        } else if responseMap[""] != nil {
            response = responseMap[""]!.popLastExcludeLastOne()!
        } else {
            assertionFailure()
        }

        delegate?.request(for: url, parameters: parameters)
        if let json = response.string {
            let data = json.data(using: .utf8)!
            let object = try! JSONSerialization.jsonObject(with: data, options: [])
            let result: Result<Any, AFError>
            let responseToSend: AFDataResponse<Any>

            if response.error == nil {
                result = Result<Any, AFError>.success(object)
                responseToSend = AFDataResponse(request: URLRequest(url: URL(string: url)!), response: HTTPURLResponse(), data: data, metrics: nil, serializationDuration: 0, result: result)
            } else {

                result = Result<Any, AFError>.failure(AFError.sessionTaskFailed(error: response.error!))
                let url = URL(string: url)!
                let httpRespnse = HTTPURLResponse(url: url, statusCode: response.error?.code ?? 0, httpVersion: nil, headerFields: nil)

                responseToSend = AFDataResponse(request: URLRequest(url: url), response: httpRespnse, data: data, metrics: nil, serializationDuration: 0, result: result)
            }

            completion(responseToSend)
        } else {
            let result = Result<Any, AFError>.failure(AFError.sessionTaskFailed(error: response.error!))
            let responseToSend = DataResponse(request: URLRequest(url: URL(string: url)!), response: HTTPURLResponse(), data: "{}".data(using: .utf8)!, metrics: nil, serializationDuration: 0, result: result)
            completion(responseToSend)
        }

        return nil
    }
}

extension Array {
    mutating func popLastExcludeLastOne() -> Element? {
        if count > 1 {
            return popLast()
        } else {
            return first
        }
    }
}
