//
//  MainFlowController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

final class MainFlowController: NavigationController {
    var service: ServicesProtocol?

    var didShowIntro: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: .didShowIntro)
        }

        get {
            UserDefaults.standard.bool(forKey: .didShowIntro)
        }
    }

    init() {
        service = BaseServices(user: User.serviceUser)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureRootViewController()
        if !didShowIntro {
            showIntro()
        }
        if service?.isAuthenticated == false {
            showLogin()
        }
    }
}

extension MainFlowController {
    func setupNavigationBar() {
        guard let service = service, let gender = service.user.gender else { return }
        navigationBar.tintColor = UIColor.mainColor(for: gender)
        navigationBar.barTintColor = UIColor.mainColor(for: gender)
    }
}

extension MainFlowController {
    fileprivate func configureRootViewController() {
        guard let service = service else { return }
        setupNavigationBar()
        let vc = HomeViewController(service: service)
        viewControllers = [vc]
    }

    fileprivate func showIntro() {
        guard let service = service else { return }
        let introVC = IntroViewController(service: service)
        introVC.delegate = self
        present(introVC, animated: true, completion: nil)
    }

    fileprivate func showLogin() {
        guard let service = service, !service.isAuthenticated else { return }
        let flow = LoginFlowController(service: service)
        flow.delegate = self
        let vc = flow.initial(with: self)
        present(vc, animated: true, completion: nil)
    }
}

extension MainFlowController: IntroViewControllerDelegate {
    func introViewControllerDidFinishShowing(_ introVC: IntroViewController) {
        introVC.dismiss(animated: true) { [weak self] in
            self?.didShowIntro = true
            self?.showLogin()
        }
    }
}

extension MainFlowController: LoginFlowControllerDelegate {
    func loginFlow(_ flow: LoginFlowController, didRegisterSeccessfullyWithUser: User) {
        flow.navigationController?.dismiss(animated: true, completion: nil)
        setupNavigationBar()
    }
}
