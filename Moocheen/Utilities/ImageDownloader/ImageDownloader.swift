//
//  ImageDownloader.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import Kingfisher

class ImageDownloader {
    static let shared = ImageDownloader()
    private init() { }

    func store(_ image: UIImage, cacheKey: String, cacheType: ImageDownloaderCacheType = .memory, completion: (() -> Void)? = nil) {
        let cache = ImageCache.default
        switch cacheType {
        case .none:
            completion?()
        case .memory:
            cache.store(image, forKey: cacheKey)
            completion?()
        case .disk:
            guard let data = image.jpegData(compressionQuality: 1) else { return }
            cache.storeToDisk(data, forKey: cacheKey) { _ in
                completion?()
            }
        }
    }

    func image(cacheKey: String, completion: @escaping (Result<UIImage?, Error>) -> Void) {
        let cache = ImageCache.default
        cache.retrieveImage(forKey: cacheKey) { (result) in
            switch result {
            case let .success(value):
                completion(.success(value.image))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    func remove(cahceKey: String) {
        let cache = ImageCache.default
        cache.removeImage(forKey: cahceKey)
    }

    @discardableResult
    func downloadAndCacheImage(url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) -> DownloadTask? {
        return KingfisherManager.shared.downloader.downloadImage(with: url) { (result) in
            switch result {
            case let .success(image):
                completion(.success(image.image))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    @discardableResult
    func downloadImage(url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) -> DownloadTask? {
        return Kingfisher.ImageDownloader.default.downloadImage(with: url) { (result) in
            switch result {
            case let .success(image):
                completion(.success(image.image))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}

extension UIImageView {
    @discardableResult
    func downloadImage(lightUrl lightUrlString: String, darkUrl darkUrlString: String, placeholder: UIImage? = nil, completion: ((Result<UIImage, Error>) -> Void)? = nil) -> DownloadTask? {
        var options = KingfisherOptionsInfo()
        options.append(.waitForCache)
        options.append(.alsoPrefetchToMemory)

        let darkResource = ImageResource(downloadURL: URL(string: darkUrlString) ?? URL(fileURLWithPath: ""), cacheKey: darkUrlString.sha256)
        let lightResource = ImageResource(downloadURL: URL(string: lightUrlString) ?? URL(fileURLWithPath: ""), cacheKey: lightUrlString.sha256)

        UIImageView().kf.setImage(with: isDarkMode ? lightResource : darkResource, placeholder: placeholder, options: options)

        return self.kf.setImage(with: isDarkMode ? darkResource : lightResource, placeholder: placeholder, options: options) { (result) in
            switch result {
            case let .success(value):
                completion?(.success(value.image))
            case let .failure(error):
                completion?(.failure(error))
            }
        }
    }

    @discardableResult
    func downloadImage(url urlString: String, cacheType: ImageDownloaderCacheType = .memory, cacheKey: String? = nil, placeholder: UIImage? = nil, completion: ((Result<UIImage, Error>) -> Void)? = nil) -> DownloadTask? {
        guard let url = URL(string: urlString) else {
//            self.image = .defaultPlaceholder
            completion?(.failure(NSError()))
            return nil
        }

        var options = KingfisherOptionsInfo()

        switch cacheType {
        case .none:
            options.append(.forceRefresh)
        case .memory:
            options.append(.cacheMemoryOnly)
            options.append(.fromMemoryCacheOrRefresh)
        case .disk:
            options.append(.waitForCache)
            options.append(.alsoPrefetchToMemory)
        }

        let resource = ImageResource(downloadURL: url, cacheKey: cacheKey ?? url.absoluteString.sha256)

        return self.kf.setImage(with: resource, placeholder: placeholder, options: options) { (result) in
            switch result {
            case let .success(value):
                completion?(.success(value.image))
            case let .failure(error):
                completion?(.failure(error))
            }
        }
    }
}

enum ImageDownloaderCacheType {
    case none
    case memory
    case disk
}

extension UIImage {
//    static var defaultPlaceholder = IconBuilder(.noPic).build()
}
