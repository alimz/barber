//
//  TextFieldWithIcon.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class TextFieldWithIcon: UIView, ComponentProtocol {
    var gender: Gender = .female

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var iconImageView: UIImageView!

    @IBInspectable var text: String? {
        set { textField.text = newValue }
        get { textField.text }
    }

    @IBInspectable var placeholder: String? {
        set { textField.placeholder = newValue }
        get { textField.placeholder }
    }

    @IBInspectable var iconImage: UIImage? {
        set { iconImageView.image = newValue }
        get { iconImageView.image }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("TextFieldWithIcon", owner: self, options: nil)
        contentView.fixInView(self)
    }

    func fill(placeholder: String?, icon: UIImage?) {
        DispatchQueue.main.async {
            self.textField?.placeholder = placeholder
            self.iconImageView?.image = icon
        }
    }

    func set(text: String?) {
        textField.text = text
    }

    func set(icon: UIImage?) {
        iconImageView.image = icon
    }
}
