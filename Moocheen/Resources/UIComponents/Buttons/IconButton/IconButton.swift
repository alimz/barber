//
//  IconButton.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class IconButton: UIButton, ComponentProtocol {

    let gradientLayer = CAGradientLayer()
    var gender: Gender = .female

    override var isSelected: Bool {
        didSet {
            gradientBackground()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit() {
        backgroundColor = .white
        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont(.light, withSize: 14)
        setTitleColor(UIColor(namedColor: .GreyPrimary), for: .normal)
        setTitleColor(UIColor(namedColor: .WhitePrimary), for: .selected)
        cornerRadius = frame.height / 2
        layer.masksToBounds = true

        imageEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)

        shadowColor = UIColor(namedColor: .GreySecondary)
        shadowRadius = 2
        shadowOffset = .zero
        shadowOpacity = 0.15
    }

    fileprivate func gradientBackground() {
        if isSelected {
            gradientLayer.frame = bounds
            gradientLayer.startPoint = CGPoint(x: 0.0,y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0,y: 0.5)
            gradientLayer.colors = gender.gradientColors.map { $0.cgColor }
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
}
