//
//  BarberImageItemCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/21/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BarberImageItemCell: UICollectionViewCell, HorizontalListCell, ComponentProtocol {

    @IBOutlet private weak var imageView: UIImageView!

    func fill(with item: HorizontalListItem, gender: Gender) {
        guard let imageURL = (item as? String)?.imageDownloadURL else { return }
        imageView.downloadImage(url: imageURL)
    }
}
