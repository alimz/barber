//
//  UserHeaderViewTests.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class UserHeaderViewTests: MoocheenTestCase {
    func testComponents() {
        let fakeBarber = BarberProfile(firstName: "Ali", lastName: "Moazenzadeh", gender: .male, address: "", score: 5, profileImage: "", sampleList: nil, workingHours: nil, barberName: "Barbershop", services: nil)
        let view = UserHeaderView(frame: CGRect(x: 0, y: 0, width: 320, height: 80))
        view.fill(with: fakeBarber)

        XCTAssertEqual(view.barbershopNameLabel.text, "Barbershop")
        XCTAssertEqual(view.barberNameLabel.text, fakeBarber.fullName)
    }
}
