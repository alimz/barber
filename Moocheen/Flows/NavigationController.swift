//
//  NavigationController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    var flowControlers: [BaseFlowController]?

    convenience init(rootViewController: UIViewController, flowControllers: [BaseFlowController]?) {
        self.init()
        flowControlers = flowControllers
        viewControllers = [rootViewController]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(namedColor: .BlackPrimary),
                                                            .font: UIFont(DefaultFontType.medium, withSize: 17)]

        let gender = (UIApplication.topViewController() as? BaseViewController)?.service.user.gender ?? .female
        navigationBar.barTintColor = UIColor.mainColor(for: gender)
        navigationBar.tintColor = UIColor(namedColor: .WhitePrimary)
        navigationBar.isTranslucent = false
        modalPresentationStyle = .formSheet
    }

    func childDidFinish(_ child: BaseFlowController?) {
        for (index, flow) in flowControlers?.enumerated() ?? [].enumerated() where flow == child {
            flowControlers?.remove(at: index)
            break
        }
    }

    func popLast(_ k: Int = 1, animated: Bool) {
        let flows = viewControllers.filter({($0 as? BaseViewController)?.finishDelegate != nil })
        if let rootOfFlow = flows[safe: flows.count - k] {
            popToViewController(rootOfFlow, animated: true)
        } else {
            _ = popToRootViewController(animated: animated)
        }
    }

    @discardableResult
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        if let firstVC = flowControlers?.first { flowControlers = [firstVC] }
        return super.popToRootViewController(animated: animated)
    }
}

