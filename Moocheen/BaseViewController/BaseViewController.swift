//
//  BaseViewController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var service: ServicesProtocol
    weak var finishDelegate: FinishFlowDelegate?

    init(service: ServicesProtocol) {
        self.service = service
        super.init(nibName: nil, bundle: nil)
    }

    init(service: ServicesProtocol, withNib nibName: String) {
        self.service = service
        super.init(nibName: nibName, bundle: .main)
    }

    required init?(coder _: NSCoder) {
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        showCancelButton()
        view.backgroundColor = UIColor(namedColor: .Background)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        setGender()
    }

    func setGender() {
        for view in view.subviews.filter({ $0 is ComponentProtocol }) {
            (view as! ComponentProtocol).changeGender(to: service.user.gender ?? .female)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParent || isBeingDismissed {
            finishDelegate?.didFinishFlow(self)
        }
    }

    func showCancelButton() {
        if isOpenModally {
            let rightButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(cancel))
            navigationItem.rightBarButtonItem = rightButton
        }
    }

    @objc private func cancel() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}

enum MessageViewStyle {
    case alert
    case snackBar
}

extension UIViewController {
    var isPortrait: Bool {
        return view.frame.size.width < view.frame.size.height
    }

    var isOpenModally: Bool {
        return navigationController != nil &&
            navigationController!.children.count == 1 &&
            navigationController?.presentingViewController?.presentedViewController == navigationController
    }

    var isRoot: Bool {
        guard let nv = navigationController else { return false}
        return nv.viewControllers.first == self
    }

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    func changeGender(to gender: Gender) {
        let subviews = view.subviews.compactMap { $0 as? ComponentProtocol }
        subviews.forEach { $0.changeGender(to: gender) }
    }

    func barButton(title: String, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 24)
        button.setTitle(title, for: .normal)
        button.tintColor = UIColor(namedColor: .BrandPrimary)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.font = UIFont(.regular, withSize: 14)
        button.titleLabel?.lineBreakMode = .byClipping
        button.titleLabel?.numberOfLines = 1
        button.addTarget(self, action: action, for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 5)
        button.contentHorizontalAlignment = .right

        return UIBarButtonItem(customView: button)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
