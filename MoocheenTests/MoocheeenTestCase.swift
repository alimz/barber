//
//  MoocheTestCase.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class MoocheenTestCase: XCTestCase {

    let expectation = XCTestExpectation(description: "Get the result")
    static var user: User?
    static var gender: Gender?

    override class func setUp() {
        user = User.savedUser
        gender = User.savedUser?.gender
    }

    override class func tearDown() {
        let exp = XCTestExpectation(description: "tearDown wait")
        user?.save()
        if let gender = gender {
            user?.gender = gender
        }
        XCTWaiter().wait(for: [exp], timeout: 1)

        print("--> \(String.init(describing: self)) \(String(describing: UIApplication.topViewController()))")
    }
}
