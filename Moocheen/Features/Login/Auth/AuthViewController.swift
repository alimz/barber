//
//  AuthViewController.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/12/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol AuthViewControllerDelegate: class {
    func authViewController(_ vc: AuthViewController, didFinishSuccessfullyWithToken token: String)
}

class AuthViewController: BaseViewController {

    @IBOutlet weak var codeTextField: CurvedTextField!
    @IBOutlet weak var verifyButton: CurvedButton!

    var tempUser: User
    weak var delegate: AuthViewControllerDelegate?

    init(service: ServicesProtocol, tempUser: User) {
        self.tempUser = tempUser
        super.init(service: service)
    }

    required init?(coder _: NSCoder) {
        return nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeGender(to: tempUser.gender ?? .female)
    }

    fileprivate func setupUI() {
        codeTextField.fill(placeholder: "Enter code".localized)
    }

    @IBAction func authAction(_ sender: CurvedButton) {
        guard let code = codeTextField.text else { return }
        guard let phone = tempUser.phone else { return }
        service.loginAuth(phone: phone, code: code) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let authData):
                self.delegate?.authViewController(self, didFinishSuccessfullyWithToken: authData.token)
            case .failure(let error):
                print(error)
            }
        }
    }

}
