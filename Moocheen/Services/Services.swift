//
//  Services.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/14/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import Alamofire

class BaseServices: ServicesProtocol {

    var user: User {
        didSet {
            user.save()
        }
    }
    private var sessionManager: Session?

    init(user: User) {
        self.user = user
        var defaultHeaders = Alamofire.HTTPHeaders.default
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = defaultHeaders.dictionary
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        sessionManager = Session(configuration: configuration)
    }

    @discardableResult
    func sendRequest(_ url: String,
                     method: HTTPMethod = .get,
                     parameters: Parameters? = nil,
                     imageData: Data? = nil,
                     completion: @escaping (_ response: DataResponse<Any, AFError>) -> Void) -> DataRequest? {
        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        var finalHeaders: [String: String] = [:]

        if let token = user.authenticationToken {
            finalHeaders.merge(["Authorization": "Token \(token)"]) { _, new in new }
        }

        if let imageData = imageData {
            finalHeaders.merge(["Content-Type": "image/jpeg"]) { _, new in new }
            return sessionManager?.upload(imageData, to: url, method: method, headers: HTTPHeaders(finalHeaders)).responseJSON { response in
                completion(response)
            }
        } else {
            return sessionManager?.request(encodedUrl ?? url, method: method, parameters: parameters, encoding: method == .get ? URLEncoding.default : JSONEncoding.default, headers: HTTPHeaders(finalHeaders)).validate().responseJSON { [weak self] response in
                if response.response?.statusCode == 401 {
                    self?.user.authenticationToken = nil
                }
                completion(response)
            }
        }
    }
}
