//
//  UserHeaderView.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class UserHeaderView: UIView {

    @IBOutlet private weak var contentView: UIView!
    
    @IBOutlet weak var barberImageView: UIImageView!
    @IBOutlet weak var barbershopNameLabel: UILabel!
    @IBOutlet weak var barberNameLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    fileprivate func commonInit() {
        Bundle.main.loadNibNamed("UserHeaderView", owner: self, options: nil)
        contentView.fixInView(self)
    }

    func fill(with barber: BarberProfile) {
        if let imageURL = barber.profileImage?.imageDownloadURL {
            barberImageView.downloadImage(url: imageURL)
        }
        barbershopNameLabel.text = barber.barberName
        barberNameLabel.text = barber.fullName
    }

}
