//
//  IconBuilder.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/14/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import UIKit

class IconBuilder {
    private var image: UIImage?

    enum Icons: String, CaseIterable {
        case menBarber = "men-barber"
        case womenBarber = "women-barber"
        case scissors = "ic-scissors"
        case eyelash = "ic-eyelash"
        case makeup = "ic-makeup"
        case male = "ic-male"
        case female = "ic-female"
        case nailsService = "nails-service"
        case makeupService = "makeup-service"
        case hairService = "women-hair-service"
        case like
    }

    init(_ iconName: Icons) {
        self.image = UIImage(named: iconName.rawValue)
    }

    func set(color namedColor: ColorPalette, forceStyle style: UIColorStyle = .unspecified) -> IconBuilder? {
        guard let image = image else { return nil }
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        UIColor(namedColor: namedColor, style: style).setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: image.size.width, height: image.size.height))

        guard let mask = image.cgImage else { return nil }
        context?.clip(to: rect, mask: mask)
        context?.fill(rect)

        self.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return self
    }

    func build() -> UIImage? {
        return image
    }
}

extension Bundle {
    public var icon: UIImage? {
        if let icons = infoDictionary?["CFBundleIcons"] as? [String: Any],
            let primaryIcon = icons["CFBundlePrimaryIcon"] as? [String: Any],
            let iconFiles = primaryIcon["CFBundleIconFiles"] as? [String] {
            print(iconFiles)
        }
        return nil
    }
}
