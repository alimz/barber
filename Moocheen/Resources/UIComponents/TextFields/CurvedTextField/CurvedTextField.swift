//
//  CurvedTextField.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/12/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class CurvedTextField: UIView, ComponentProtocol {
    var gender: Gender = .female

    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var textField: UITextField!

    @IBInspectable var text: String? {
        set { textField.text = newValue }
        get { textField.text }
    }

    @IBInspectable var placeholder: String? {
        set { textField.placeholder = newValue }
        get { textField.placeholder }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("CurvedTextField", owner: self, options: nil)
        contentView.fixInView(self)
    }

    func fill(placeholder: String?) {
        DispatchQueue.main.async {
            self.textField?.placeholder = placeholder
        }
    }

    func set(text: String?) {
        textField.text = text
    }
}

