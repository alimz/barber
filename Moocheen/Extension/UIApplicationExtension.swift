//
//  UIApplicationExtension.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

extension UIApplication {
    var topMostController: UIViewController? {
        var topController = UIApplication.shared.keyWindow?.rootViewController

        if topController is UITabBarController {
            topController = (topController as! UITabBarController).selectedViewController
        }

        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }

        return topController
    }


    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
