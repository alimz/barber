//
//  HorizontalCollectionTableViewCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/20/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol HorizontalListCell: UICollectionViewCell {
    func fill(with item: HorizontalListItem, gender: Gender)
}

protocol HorizontalListItem {}

struct HorizontalListStruct {
    let title: String
    let items: [HorizontalListItem]

    static var empty: HorizontalListStruct {
        HorizontalListStruct(title: "", items: [])
    }
}

protocol HorizontalCollectionCellDelegate: class {
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didSelectItem item: HorizontalListItem)
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didRequestShowMoreItemsWithData data: Any?)
}

extension HorizontalCollectionCellDelegate {
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didSelectItem item: HorizontalListItem) {}
    func horizontalListCell(_ cell: HorizontalCollectionTableViewCell, didRequestShowMoreItemsWithData data: Any?) {}
}

struct HorizontalCellConfig {
    let item: HorizontalListStruct
    let collectionCellsize: CGSize
    let data: Any?
    let gender: Gender?
    let needsMoreButton: Bool

    static var empty: HorizontalCellConfig {
        HorizontalCellConfig(item: .empty, collectionCellsize: .zero, data: nil, gender: .female, needsMoreButton: false)
    }
}

class HorizontalCollectionTableViewCell: UIView, CollectionViewProtocol {

    var _collectionView: UICollectionView { collectionView }
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet private weak var moreItemsButton: CurvedButton!

    weak var delegate: HorizontalCollectionCellDelegate?
    var collectionCellIdentifier: String = ""

    private var config = HorizontalCellConfig.empty {
        didSet { reloadData() }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("HorizontalCollectionTableViewCell", owner: self, options: nil)
        contentView.fixInView(self)
    }

    func setupCell(collectionCellIdentifier: String) {
        self.collectionCellIdentifier = collectionCellIdentifier
        setupCollectionView(cellIdentifiers: [collectionCellIdentifier])
    }

    func fill(config: HorizontalCellConfig) {
        self.config = config
        moreItemsButton.isHidden = !config.needsMoreButton
        titleLable.text = config.item.title
    }

    @IBAction fileprivate func moreItemsAction(_ sender: UIButton) {
        delegate?.horizontalListCell(self, didRequestShowMoreItemsWithData: config.data)
    }
}

extension HorizontalCollectionTableViewCell: CollectionViewDatasourceDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return config.item.items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! HorizontalListCell
        let item = config.item.items[indexPath.item]
        cell.fill(with: item, gender: config.gender ?? .female)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = config.item.items[indexPath.item]
        delegate?.horizontalListCell(self, didSelectItem: item)
    }
}

extension HorizontalCollectionTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return config.collectionCellsize
    }
}
