//
//  ComponentProtocol.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

typealias ComponentViewContorller = ComponentProtocol & BaseViewController

protocol ComponentProtocol {
    func changeGender(to gender: Gender)
    func gender(didChangeTo gender: Gender)
}

extension ComponentProtocol {
    func changeGender(to gender: Gender) {}
    func gender(didChangeTo gender: Gender) {}
}

extension ComponentProtocol where Self: UIView {
    func changeGender(to gender: Gender) {
        let to = gender == .male ? 5 : frame.height / 2
        UIViewPropertyAnimator(duration: 0.1, curve: .easeIn) {
            self.layer.masksToBounds = true
            self.layer.cornerRadius = to
        }.startAnimation()
        self.gender(didChangeTo: gender)
    }
}

extension ComponentProtocol where Self: UICollectionViewCell {
    func changeGender(to gender: Gender) {
        let to: CGFloat = gender == .male ? 5 : 15
        UIViewPropertyAnimator(duration: 0.1, curve: .easeIn) {
            self.layer.masksToBounds = true
            self.layer.cornerRadius = to
        }.startAnimation()
        self.gender(didChangeTo: gender)
    }
}
