//
//  TableViewProtocol.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

typealias TableViewDatasourceDelegate = UITableViewDataSource & UITableViewDelegate

protocol TableViewProtocol {
    var _tableView: UITableView { get }

    func setupTableView(cellIdentifiers: [String])
    func setupTableViewCells(cellIdentifiers: [String])
    func reloadData()
}

extension TableViewProtocol where Self: TableViewDatasourceDelegate {
    func setupTableView(cellIdentifiers: [String]) {
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.tableFooterView = UIView()
        _tableView.separatorStyle = .none
        setupTableViewCells(cellIdentifiers: cellIdentifiers)
    }

    func setupPullToRefresh(selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: selector, for: .valueChanged)
        _tableView.refreshControl = refreshControl
    }

    func setupTableViewCells(cellIdentifiers: [String]) {
        for identifier in cellIdentifiers {
            _tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        }
    }

    func reloadData() {
        DispatchQueue.main.async {
            self._tableView.reloadData()
        }
    }
}
