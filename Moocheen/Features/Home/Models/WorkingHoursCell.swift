//
//  WorkingHoursCell.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class WorkingHoursCell: UITableViewCell {

    @IBOutlet private weak var horizontalList: HorizontalCollectionTableViewCell!

    override func awakeFromNib() {
        super.awakeFromNib()
        horizontalList.setupCell(collectionCellIdentifier: "WorkingHourItemCell")
    }

    func fill(with item: WorkingHoursModel, gender: Gender?) {
        let config = HorizontalCellConfig(item: HorizontalListStruct(title: item.title, items: item.items), collectionCellsize: CGSize(width: 128, height: 100), data: nil, gender: gender, needsMoreButton: false)
        horizontalList.fill(config: config)
    }

}
