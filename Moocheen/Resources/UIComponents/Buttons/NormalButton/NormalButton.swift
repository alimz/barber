//
//  NormalButton.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class NormalButton: UIButton, ComponentProtocol {
    var gender: Gender = .female

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    fileprivate func commonInit() {
        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont(.regular, withSize: 15)
        contentEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
        setTitleColor(UIColor(namedColor: .WhitePrimary), for: .normal)
        backgroundColor = UIColor(namedColor: gender == .female ? .FemalePink : .MaleBlue)
        shadowColor = UIColor(namedColor: .Background)
        cornerRadius = 4
        shadowRadius = 5
    }

    func gender(didChangeTo gender: Gender) {
        backgroundColor = UIColor(namedColor: gender == .female ? .FemalePink : .MaleBlue)
    }
}
