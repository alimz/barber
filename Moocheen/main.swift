//
//  main.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import UIKit

UserDefaults.standard.set(true, forKey: .appleTextDirection)
UserDefaults.standard.set(true, forKey: .nSForceRightToLeftWritingDirection)
UserDefaults.standard.set(["fa_IR"], forKey: .appleLanguages)

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    nil,
    NSStringFromClass(AppDelegate.self)
)
