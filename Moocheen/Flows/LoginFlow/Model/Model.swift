//
//  Model.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 4/26/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    let token: String
}
