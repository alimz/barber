//
//  ServiceURLs.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 4/26/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

enum BaseURL: String {
    case gateway = "http://5.253.24.199:8000/client/"
}

enum RequestURL {
    case register(String)
    case loginAuth
    case getHomePage
    case getBarberPage
}

extension RequestURL {
    var path: String {
        switch self {
        case .register(let phone):
            return BaseURL.gateway.rawValue.appending("login/\(phone)")
        case .loginAuth:
            return BaseURL.gateway.rawValue.appending("login/")
        case .getHomePage:
            //            return BaseURL.gateway.rawValue.appending("home_page/")
            return "http://192.168.1.4:8000/main-page.json"
        case .getBarberPage:
            //            return BaseURL.gateway.rawValue.appending("barber_profile/")
            return "http://192.168.1.4:8000/barber.json"
        }
    }
}
