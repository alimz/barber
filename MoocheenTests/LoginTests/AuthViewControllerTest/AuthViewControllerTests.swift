//
//  AuthViewControllerTests.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
@testable import Moocheen
import XCTest

class AuthViewControllerTests: MoocheenTestCase, AuthViewControllerDelegate {

    func authViewController(_ vc: AuthViewController, didFinishSuccessfullyWithToken token: String) {
        expectation.fulfill()
    }

    var vc: AuthViewController?

    override func setUp() {
        let mockService = ServicesMock(response: ("{\"token\":\"fake_token\"}", nil))
        let fakeUser = User(authenticationToken: "fake_token", gender: .male, phone: "09123456789")
        vc = AuthViewController(service: mockService, tempUser: fakeUser)
        vc?.delegate = self
        let _ = vc!.view
    }

    func testInitWithCoder() {
        let archiver = NSKeyedArchiver(requiringSecureCoding: true)
        let brandVC = AuthViewController(coder: archiver)
        XCTAssertNil(brandVC)
    }

    func testAuth() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.vc?.authAction(CurvedButton())
        }
        wait(for: [expectation], timeout: 2)
    }
}
