//
//  CurvedButton.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/11/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class CurvedButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit() {
        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont(.light, withSize: 14)
        cornerRadius = frame.height / 2
        layer.masksToBounds = true

        shadowColor = UIColor(namedColor: .GreySecondary)
        shadowRadius = 2
        shadowOffset = .zero
        shadowOpacity = 0.15

        contentEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        cornerRadius = frame.height / 2
    }
}
