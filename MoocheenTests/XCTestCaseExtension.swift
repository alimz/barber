//
//  XCTestCaseExtension.swift
//  MoocheenTests
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    func loadMock(fileName: String, ofType: String = "json") -> String? {
        guard let path = Bundle(for: type(of: self)).path(forResource: fileName, ofType: ofType) else {
            return nil
        }
        return try? String(contentsOfFile:path, encoding: .utf8)
    }
}
