//
//  CollectionViewProtocol.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

typealias CollectionViewDatasourceDelegate = UICollectionViewDelegate & UICollectionViewDataSource

protocol CollectionViewProtocol {
    var _collectionView: UICollectionView { get }

    func setupCollectionView(cellIdentifiers: [String])
    func setupCollectionViewCells(cellIdentifiers: [String])
    func reloadData()
}

extension CollectionViewProtocol where Self: CollectionViewDatasourceDelegate {
    func setupCollectionView(cellIdentifiers: [String]) {
        _collectionView.delegate = self
        _collectionView.dataSource = self
        setupCollectionViewCells(cellIdentifiers: cellIdentifiers)
    }

    func setupCollectionViewCells(cellIdentifiers: [String]) {
        for identifier in cellIdentifiers {
            _collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        }
    }

    func reloadData() {
        DispatchQueue.main.async {
            self._collectionView.reloadData()
        }
    }
}
