//
//  BarberServiceModel.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/21/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

struct BarberServiceItem: HorizontalListItem, Codable {
    let imageURL: String?
    let title: String?
    let serviceID: String?

    enum CodingKeys: String, CodingKey {
        case imageURL = "url"
        case title = "name"
        case serviceID = "service_number"
    }
}

struct BarberServiceModel {
    let title: String
    let items: [BarberServiceItem]
}
