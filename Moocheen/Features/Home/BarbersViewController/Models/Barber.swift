//
//  Barber.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation
import CoreLocation

enum BarbersType: String {
    case bestBarbers = "best_barbers"
    case closestBarbers = "closest_barbers"
}

struct Barber: Codable {
    let id: String?
    let name: String?
    let imageURL: String?
    let distance: Double?

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageURL = "image_url"
        case distance
    }
}

extension Barber: HorizontalListItem {}

struct BarbersListItem: Codable {
    let title: String
    let barbers: [Barber]
}
