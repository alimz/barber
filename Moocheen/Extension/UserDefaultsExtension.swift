//
//  UserDefaultsExtension.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/14/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

extension UserDefaults {
    enum Keys: String {
        case appleTextDirection = "AppleTextDirection"
        case nSForceRightToLeftWritingDirection = "NSForceRightToLeftWritingDirection"
        case appleLanguages = "AppleLanguages"

        case user
        case didShowIntro
    }

    func updateKeyValues(_ dic: [Keys: Any?]) {
        for (key, value) in dic {
            if value == nil {
                removeObject(forKey: key.rawValue)
            } else {
                UserDefaults.standard.setValue(value, forKey: key.rawValue)
            }
        }
        UserDefaults.standard.synchronize()
    }

    func bool(forKey defaultName: Keys) -> Bool {
        return UserDefaults.standard.bool(forKey: defaultName.rawValue)
    }

    func set(_ value: Any?, forKey defaultName: Keys) {
        UserDefaults.standard.set(value, forKey: defaultName.rawValue)
    }

    func string(forKey defaultName: Keys) -> String? {
        return UserDefaults.standard.string(forKey: defaultName.rawValue)
    }

    func integer(forKey defaultName: Keys) -> Int {
        return UserDefaults.standard.integer(forKey: defaultName.rawValue)
    }

    func value(forKey defaultName: Keys) -> Any? {
        return UserDefaults.standard.value(forKey: defaultName.rawValue)
    }

    func object(forKey defaultName: Keys) -> Any? {
        return UserDefaults.standard.object(forKey: defaultName.rawValue)
    }

    func removeObject(forKey defaultName: Keys) {
        UserDefaults.standard.removeObject(forKey: defaultName.rawValue)
    }
}

