//
//  BarberServicesCell.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 6/20/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BarberServicesCell: UITableViewCell {

    @IBOutlet weak var horizontalList: HorizontalCollectionTableViewCell!

    override func awakeFromNib() {
        super.awakeFromNib()
        horizontalList.setupCell(collectionCellIdentifier: "ServiceCollectionViewCell")
    }

    func fill(with item: BarberServiceModel, gender: Gender?) {
        let config = HorizontalCellConfig(item: HorizontalListStruct(title: item.title, items: item.items), collectionCellsize: CGSize(width: 100, height: 100), data: nil, gender: gender, needsMoreButton: false)
        horizontalList.fill(config: config)
    }
}
