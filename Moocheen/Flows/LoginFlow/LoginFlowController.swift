//
//  LoginFlowController.swift
//  Barber
//
//  Created by Ali`s Macbook Pro on 2/28/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

protocol LoginFlowControllerDelegate: class {
    func loginFlow(_ flow: LoginFlowController, didRegisterSeccessfullyWithUser: User)
}

class LoginFlowController: BaseFlowController {

    weak var delegate: LoginFlowControllerDelegate?

    override init(service: ServicesProtocol) {
        super.init(service: service)
    }

    func initial(with navigationController: NavigationController) -> UIViewController {
        let vc = LoginViewController(service: service)
        vc.delegate = self
        self.navigationController = navigationController
        let nv = NavigationController(rootViewController: vc, flowControllers: [self])
        self.navigationController = nv
        return nv
    }
}

extension LoginFlowController: LoginViewControllerDelegate {
    func loginViewController(_ vc: LoginViewController, didLoginWithTempUser user: User) {
        guard let nv = navigationController else { return }
        let vc = AuthViewController(service: service, tempUser: user)
        vc.delegate = self
        nv.pushViewController(vc, animated: true)
    }
}

extension LoginFlowController: AuthViewControllerDelegate {
    func authViewController(_ vc: AuthViewController, didFinishSuccessfullyWithToken token: String) {
        let user = User(authenticationToken: token, gender: vc.tempUser.gender, phone: vc.tempUser.phone)
        service.user = user
        delegate?.loginFlow(self, didRegisterSeccessfullyWithUser: user)
    }
}
