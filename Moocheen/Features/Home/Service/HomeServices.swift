//
//  HomeServices.swift
//  Moocheen
//
//  Created by Ali Moazenzadeh on 7/16/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import Foundation

extension BaseServices {
    func getBestBarbers(offset: Int, completion: @escaping (Result<[Barber], Error>) -> Void) {

    }

    func getClosestBarbers(offset: Int, completion: @escaping (Result<[Barber], Error>) -> Void) {

    }

    func getHomePage(completion: @escaping (Result<HomeModel, Error>) -> Void) {
        sendRequest(RequestURL.getHomePage.path) { result in
            guard let data = result.data else {
                completion(.failure(result.error!))
                return
            }
            do {
                let result: HomeModel = try JSONDecoder().decode(HomeModel.self, from: data)
                completion(.success(result))
            } catch {
                completion(.failure(error))
            }
        }
    }

    func serachBarbers(name: String, completion: @escaping (Result<[Barber], Error>) -> Void) {

    }

    func filterBarbers(filter: FilterModel, completion: @escaping (Result<[Barber], Error>) -> Void) {

    }
}
