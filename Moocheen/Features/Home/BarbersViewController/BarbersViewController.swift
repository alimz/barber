//
//  BarbersViewController.swift
//  Moocheen
//
//  Created by Ali`s Macbook Pro on 5/19/20.
//  Copyright © 2020 alimz. All rights reserved.
//

import UIKit

class BarbersViewController: ComponentViewContorller, TableViewProtocol {

    @IBOutlet private weak var tableView: UITableView!
    
    var _tableView: UITableView { tableView }
    var barbersListItems = [BarbersListItem]() {
        didSet { reloadData() }
    }

    var homeModel: HomeModel? {
        didSet { configSections() }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home".localized
        setupTableView(cellIdentifiers: ["BarbersListCell"])
        setupPullToRefresh(selector: #selector(getHomePage))
        getHomePage()
    }

    @objc fileprivate func getHomePage() {
        service.getHomePage { [weak self] result in
            switch result {
            case .success(let homePage):
                self?.homeModel = homePage
            case .failure(let error):
                print(error.localizedDescription)
            }
            self?.tableView.refreshControl?.endRefreshing()
        }
    }

    fileprivate func configSections() {
        var sections = [BarbersListItem]()
        guard let homeModel = homeModel else { return }

        if let bestBarbers = homeModel.bestBarbers {
            sections.append(.init(title: "Best barbers".localized, barbers: bestBarbers))
        }

        if let closestBarbers = homeModel.closestBarbers {
            sections.append(.init(title: "Closest barbers".localized, barbers: closestBarbers))
        }

        barbersListItems = sections
    }
}

extension BarbersViewController: TableViewDatasourceDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return barbersListItems.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarbersListCell") as! BarbersListCell
        let item = barbersListItems[indexPath.row]
        cell.fill(with: item, gender: service.user.gender)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension BarbersViewController: BarbersListCellDelegate {
    func babersListCell(_ cell: BarbersListCell, didSelectBarber barber: Barber) {
        guard let id = barber.id else { return }
        let nv = NavigationController(rootViewController: BarberViewController(barberID: id, service: service))
        present(nv, animated: true, completion: nil)
    }

    func babersListCell(_ cell: BarbersListCell, didRequestShowMoreItemsForType type: BarbersType) {
        print("Request show more of \(type.rawValue)")
    }
}
